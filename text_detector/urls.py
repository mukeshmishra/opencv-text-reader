from django.urls import include, path, re_path
from text_detector import views
from django.conf.urls import url


app_name = "main"

urlpatterns = [
    path('', views.index, name='index'),
    path('detect/', views.detect, name='detect'),
   



]
