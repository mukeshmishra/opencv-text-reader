from django.apps import AppConfig


class TextDetectorConfig(AppConfig):
    name = 'text_detector'
